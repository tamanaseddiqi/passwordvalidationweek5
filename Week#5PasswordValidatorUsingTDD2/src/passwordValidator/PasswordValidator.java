package passwordValidator;

/**
 *
 * @author Tamana Seddiqi - 991528861
 *
 */

public class PasswordValidator {
	//first refactor - creating a fixed value
	private static int MIN_LENGTH = 8;

	//1st refactor - creating a fixed variable for min digits
	private static int MIN_DIGITS = 2;



	//Check if the password has both upper and lower case chars
	public static boolean hasValidCaseChars(String password){

		return password != null && password.matches("^.*[a-z].*$") && password.matches("^.*[A-Z].*$");
	}




	/*
	 * This returns true for initial success test
	 */
//	public static boolean hasAtleastTwoDigits(String password){
//		return true;
//	}

	/*
	 * Checks for a digit number and counts it one by one from the string
	 * and returns the result accordingly
	 */
//	public static boolean hasAtleastTwoDigits(String password){
//		int count = 0;
//		for(int i = 0; i < password.length(); i++){
//			if(Character.isDigit(password.charAt(i))){
//				count++;
//			}
//		}
//		if(count >= 2){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}


	/*
	 * 2nd refactor - Checks for a digit number and counts it one by one from the string
	 * and returns the result accordingly
	 */
	public static boolean hasAtleastTwoDigits(String password){
		int count = 0;
		for(int i = 0; i < password.length(); i++){
			if(Character.isDigit(password.charAt(i))){
				count++;
			}
		}
		return count >= MIN_DIGITS;
	}


	//5th refactor - it looks for spaces befor, after and in between the password string
	//if indexof method returns -1, it doesn't have any spaces so it is true then it checks for the stirng length
	public static boolean isValidLenght(String password){
		return (password.indexOf(" ") == -1) && (password.length() >= MIN_LENGTH);
	}



	//first code
//	public static boolean isValidLenght(String password){
//	if(password.length() > 7){
//		return true;
//	}else{
//		return false;
//	}
//}

	//1st refactor
//	public static boolean isValidLenght(String password){
//		if(password.length() >= MIN_LENGTH){
//			return true;
//		}else{
//			return false;
//		}
//	}

//	//2nd refactor
//	public static boolean isValidLenght(String password){
//		return password.length() >= MIN_LENGTH;
//	}



	//3rd refactor - check for any spaces before and after
//	public static boolean isValidLenght(String password){
//		return password.trim().length() >= MIN_LENGTH;
//	}

	//4th refactor - check for spaces in between, before, after characters
//	public static boolean isValidLenght(String password){
//		if(password.indexOf(" ") >= 0){
//			return false;
//		}
//		return password.length() >= MIN_LENGTH;
//	}

}
