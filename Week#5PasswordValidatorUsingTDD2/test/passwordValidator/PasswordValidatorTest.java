package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *
 * @author Tamana Seddiqi - 991528861
 * Validates passwords and is being developed using TDD
 *
 */

public class PasswordValidatorTest {


//	@Test
//	public void testHasValidCaseCharsRegular(){
//		fail("Ivalid case chars");
//	}

	@Test
	public void testHasValidCaseCharsRegular(){
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("AasdddAAAAA"));
	}

	@Test
	public void testHasValidCaseCharsBoundaryIn(){
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}

	@Test
	public void testHasValidCaseCharsExceptionNumbers(){
		assertFalse("Invalid case chars",  PasswordValidator.hasValidCaseChars("12345"));
	}

	@Test
	public void testHasValidCaseCharsExceptionSpecial(){
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("!@#$%^"));
	}

	@Test
	public void testHasValidCaseCharsBlank(){
		assertFalse("Invalid case chars",  PasswordValidator.hasValidCaseChars(""));
	}

	@Test
	public void testHasValidCaseCharsExceptionNull(){
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}

	@Test
	public void testHasValidCaseCharsBoundaryOutUpperCase(){
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("AAAAA"));
	}

	@Test
	public void testHasValidCaseCharsBoundaryOutLowerCase(){
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("aaaaaa"));
	}



	/**************                              **************/



//	@Test
//	public void testHasAtleastTwoDigitsRegular() {
//		fail("Invalid password");
//	}


	@Test
	public void testHasAtleastTwoDigitsRegular() {
		boolean result = PasswordValidator.hasAtleastTwoDigits("Tamana123");
		assertTrue("Invalid password", result);
	}


//	@Test
//	public void testHasAtleastTwoDigitsException() {
//		fail("Invalid password");
//	}


	@Test
	public void testHasAtleastTwoDigitsException() {
		boolean result = PasswordValidator.hasAtleastTwoDigits("Tamanaseddiqi");
		assertFalse("Invalid password", result);
	}


//	@Test
//	public void testHasAtleastTwoDigitsBoundaryIn() {
//		fail("Invalid password");
//	}

	@Test
	public void testHasAtleastTwoDigitsBoundaryIN() {
		boolean result = PasswordValidator.hasAtleastTwoDigits("Tamana12");
		assertTrue("Invalid password", result);
	}

//	@Test
//	public void testHasAtleastTwoDigitsBoundaryOut() {
//		fail("Invalid password");
//	}

	@Test
	public void testHasAtleastTwoDigitsBoundaryOut() {
		boolean result = PasswordValidator.hasAtleastTwoDigits("Tamana1S");
		assertFalse("Invalid password", result);
	}




//	@Test
//	public void testIsValidLenghtRegular() {
//		fail("Invalid length");
//	}


	@Test
	public void testIsValidLenghtRegular() {
		boolean result = PasswordValidator.isValidLenght("1234567890");
		assertTrue("Invalid length", result);
	}


//	@Test
//	public void testIsValidLenghtExceptionSpaces() {
//		fail("Invalid length");
//	}


	@Test
	public void testIsValidLenghtException() {
		boolean result = PasswordValidator.isValidLenght("          t   e  s  t       ");
		assertFalse("Invalid length", result);
	}


//	@Test
//	public void testIsValidLenghtBoundaryIn() {
//		fail("Invalid length");
//	}

	@Test
	public void testIsValidLenghtBoundaryIN() {
		boolean result = PasswordValidator.isValidLenght("12345678");
		assertTrue("Invalid length", result);
	}

//	@Test
//	public void testIsValidLenghtBoundaryOut() {
//		fail("Invalid length");
//	}

	@Test
	public void testIsValidLenghtBoundaryOut() {
		boolean result = PasswordValidator.isValidLenght("1234567");
		assertFalse("Invalid length", result);
	}

}
